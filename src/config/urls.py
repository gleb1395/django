"""
URL configuration for config project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include
from django.conf import settings  # NOQA: F401
from django.conf.urls.static import static  # NOQA: F401

from basic.authentication_views import (
    UserLoginView,
    UserLogoutView,
    UserRegistrationView,
    send_test_email,
    ActivateUserViews,
)
from basic.views import UserProfileDetailView, UserProfileListView, UpdateUserProfileView
from students.views import IndexView

urlpatterns = [
    path("admin/", admin.site.urls),
    path("students/", include("students.urls")),
    path("", IndexView.as_view(), name="index"),
    path("teachers/", include("teachers.urls")),
    path("groups/", include("groups.urls")),
    path("__debug__/", include("debug_toolbar.urls")),
    path("login/", UserLoginView.as_view(), name="login"),
    path("logout/", UserLogoutView.as_view(), name="logout"),
    path("register/", UserRegistrationView.as_view(), name="register"),
    path("send_test_email/", send_test_email, name="send_test_email"),
    path("activate/<str:uuid64>/<str:token>", ActivateUserViews.as_view(), name="activate_user"),
    path("user_profile/<int:pk>", UserProfileDetailView.as_view(), name="user_profile"),
    path("user_profile_list", UserProfileListView.as_view(), name="user_profile_list"),
    path("update_user_profile/<int:pk>", UpdateUserProfileView.as_view(), name="update_user_profile"),
    path("oauth/", include("social_django.urls", namespace="social")),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

handler404 = "students.views.handling_404"
