from django.shortcuts import render  # NOQA F401

# Create your views here.
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.urls import reverse_lazy
from django.views.generic import DetailView, ListView, UpdateView

from basic.models import UserProfile


class UserProfileDetailView(LoginRequiredMixin, DetailView):
    model = UserProfile
    template_name = "common/user_profile.html"
    context_object_name = "user_profile"

    def get_queryset(self):
        queryset = UserProfile.objects.filter(id=self.kwargs["pk"])
        if queryset.exists():
            return UserProfile.objects.filter(id=self.kwargs["pk"])
        else:
            raise Http404


class UserProfileListView(LoginRequiredMixin, ListView):
    model = UserProfile
    template_name = "common/list_user_profile.html"
    context_object_name = "user_profile_list"


class UpdateUserProfileView(LoginRequiredMixin, UpdateView):
    model = UserProfile
    fields = ["city", "phone_number", "birth_date", "image_student"]
    template_name = "common/user_profile_update.html"
    queryset = UserProfile.objects.all()
    success_url = reverse_lazy("user_profile_list")
    pk_url_kwarg = "pk"
