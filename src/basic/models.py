from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models
from django.contrib.auth.models import PermissionsMixin, User
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from basic.managers import CustomerManager

from django.contrib.auth import get_user_model

from django.db.models.fields.files import ImageFieldFile

from faker import Faker
from phonenumber_field.modelfields import PhoneNumberField


class Customer(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_("name"), max_length=150, blank=True)
    last_name = models.CharField(_("surname"), max_length=150, blank=True)
    email = models.EmailField(
        _("email address"),
        blank=True,
        null=True,
        error_messages={
            "unique": _("A user with that email already exists."),
        },
    )
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. " "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    department = models.CharField(_("department"), max_length=300, blank=True, null=True)
    birth_date = models.DateField(_("birth date"), blank=True, null=True)
    photo = models.ImageField(_("photo"), blank=True, null=True, upload_to="static/media/img/")
    mobile_phone = PhoneNumberField(
        _("mobile phone "),
        unique=True,
        null=True,
        error_messages={
            "unique": _("A user with that mobile phone already exists."),
        },
    )

    objects = CustomerManager()

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "mobile_phone"
    REQUIRED_FIELDS = ["email"]

    class Meta:
        verbose_name = _("customer")
        verbose_name_plural = _("customers")

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def get_registration_age(self):
        return f"Time on site: {timezone.now() - self.date_joined}"

    def __str__(self):
        return str(self.mobile_phone)


class Person(models.Model):
    first_name = models.CharField(max_length=120, blank=True, null=True)
    last_name = models.CharField(max_length=120, blank=True, null=True)
    email = models.EmailField(max_length=150)

    class Meta:
        abstract = True


class UserProfile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    city = models.CharField(max_length=120)
    phone_number = PhoneNumberField(blank=True, null=True)
    birth_date = models.DateField(blank=True, null=True)
    user_image: ImageFieldFile = models.ImageField(blank=True, null=True, upload_to="static/user_profile/img/")
    user_type = models.CharField(max_length=120)

    def __str__(self) -> str:
        return f"{self.user}"

    @classmethod
    def create_user_profile(cls, count: int):
        faker = Faker()
        for x in range(count):
            user = User.objects.create_user(
                username=faker.simple_profile().get("username"),
                last_name=faker.last_name(),
                email=faker.email(),
                password="123456789",
            )
            user.save()

            student_profile = UserProfile.objects.create(
                user=user,
                city=faker.city(),
                phone_number=faker.phone_number(),
                birth_date=faker.date_of_birth(minimum_age=16, maximum_age=60),
            )
            student_profile.save()
