from django.conf import settings
from django.contrib.auth import login, get_user_model
from django.contrib.auth.views import LoginView, LogoutView
from django.core.mail import send_mail

from django.http import HttpRequest, HttpResponse
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import CreateView, RedirectView

from basic.forms import UserRegistrationForm
from basic.utils.token_generator import TokenGenerator
from services.emails import send_registration_email


class UserLoginView(LoginView): ...  # NOQA E701


class UserLogoutView(LogoutView):
    next_page = reverse_lazy("login")


class UserRegistrationView(CreateView):
    template_name = "registration/registration.html"
    form_class = UserRegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()

        send_registration_email(user_instance=self.object, request=self.request)

        return super().form_valid(form)


def send_test_email(request: HttpRequest) -> HttpResponse:
    send_mail(
        subject="Home work 15",
        message="Test email LMS. Gleb Dubinin",
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[settings.EMAIL_HOST_USER],
        fail_silently=settings.EMAIL_FAIL_SILENTLY,
    )
    return HttpResponse("DONE !!!")


class ActivateUserViews(RedirectView):
    url = reverse_lazy("index")

    # @method_decorator(login_required)
    # def dispatch(self, *args, **kwargs):
    #     return super().dispatch(*args, **kwargs)

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (get_user_model().DoesNotExist, ValueError, TypeError):
            return HttpResponse("Wrong data")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()
            login(request, current_user, backend="django.contrib.auth.backends.ModelBackend")
            return super().get(request, *args, **kwargs)
        return HttpResponse("Wrong data")
