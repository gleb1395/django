from uuid import uuid4

from django.db import models
from faker import Faker


class Group(models.Model):
    uuid = models.UUIDField(default=uuid4, primary_key=True, unique=True, editable=False, db_index=True)
    name_group = models.CharField(max_length=150, blank=True, null=True)
    classroom = models.PositiveSmallIntegerField(blank=True, null=True)
    start_time = models.TimeField(blank=True, null=True)
    end_time = models.TimeField(blank=True, null=True)
    weekday = models.CharField(max_length=20, blank=True, null=True)
    max_students = models.PositiveIntegerField(blank=True, null=True)

    @classmethod
    def generate_group(cls, count: int) -> None:
        faker = Faker()
        for i in range(count):
            group = Group(
                name_group=faker.word(),
                classroom=faker.pyint(min_value=1, max_value=150),
                start_time=faker.time(pattern="%H:%M:%S"),
                end_time=faker.time(pattern="%H:%M:%S"),
                weekday=faker.date_time().strftime("%A"),
                max_students=faker.pyint(min_value=10, max_value=20),
            )
            group.save()

    def __str__(self) -> str:
        return (
            f"{self.pk} {self.name_group} {self.classroom} {self.start_time} "
            f"{self.end_time} {self.weekday} {self.max_students}"
        )
