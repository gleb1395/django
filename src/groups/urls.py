from django.contrib import admin  # NOQA: F401
from django.urls import path

from groups.views import get_groups, info_group, GroupCreateView, GroupUpdateView, GroupDeleteView

app_name = "groups"

urlpatterns = [
    path("", get_groups, name="get_groups"),
    path("create_group/", GroupCreateView.as_view(), name="create_group"),
    path("update_group/<uuid:uuid>", GroupUpdateView.as_view(), name="update_group"),
    path("delete_group/<uuid:uuid>", GroupDeleteView.as_view(), name="delete_group"),
    path("info/<uuid:uuid>", info_group, name="info_group"),
]
