from uuid import uuid4

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView, UpdateView, DeleteView

from groups.forms import GroupForm
from groups.models import Group
from webargs import fields
from webargs.djangoparser import use_args


@login_required
@use_args(
    {
        "name_group": fields.Str(
            required=False,
        ),
        "start_time": fields.Time(
            required=False,
        ),
        "end_time": fields.Time(
            required=False,
        ),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_groups(request: HttpRequest, params) -> HttpResponse:
    groups = Group.objects.all()

    search_fields = ["name_group", "start_time", "end_time"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__contains": param_value})
            groups = groups.filter(or_filter)
        else:
            groups = groups.filter(**{param_name: param_value})

    return render(request, template_name="groups/templates/list_groups.html", context={"groups": groups})


class GroupCreateView(LoginRequiredMixin, CreateView):
    template_name = "groups/templates/create_groups.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:get_groups")


class GroupUpdateView(LoginRequiredMixin, UpdateView):
    template_name = "groups/templates/update_groups.html"
    form_class = GroupForm
    success_url = reverse_lazy("groups:get_groups")
    queryset = Group.objects.all()
    pk_url_kwarg = "uuid"


class GroupDeleteView(LoginRequiredMixin, DeleteView):
    model = Group
    fields = ["name_group", "classroom", "start_time", "end_time", "weekday", "max_students"]
    template_name = "groups/templates/delete_groups.html"
    queryset = Group.objects.all()
    success_url = reverse_lazy("groups:get_groups")
    pk_url_kwarg = "uuid"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        group = self.get_object()
        context["name_group"] = f"{group.name_group}"
        return context


@login_required
@csrf_exempt
def info_group(request: HttpRequest, uuid=uuid4) -> HttpResponse:
    group = get_object_or_404(Group.objects.all(), uuid=uuid)
    if request.method == "POST":
        form = GroupForm(request.POST, instance=group)
        if form.is_valid():
            form.save()
            pass
    elif request.method == "GET":
        form = GroupForm(instance=group)
    return render(
        request, template_name="groups/templates/informations_about_groups.html", context={"form": form, "group": group}
    )
