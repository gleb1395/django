from django.forms import ModelForm

from teachers.models import Teacher


class TeacherForm(ModelForm):
    class Meta:
        model = Teacher
        fields = ["first_name", "last_name", "email", "years_of_experience", "speciality", "group", "image_teacher"]
