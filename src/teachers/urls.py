from django.urls import path

from teachers.views import get_teachers, CreateTeacherView, DeleteTeacherView, UpdateTeacherView

app_name = "teachers"

urlpatterns = [
    path("", get_teachers, name="get_teachers"),
    path("create_teachers/", CreateTeacherView.as_view(), name="create_teachers"),
    path("update_teachers/<uuid:uuid>", UpdateTeacherView.as_view(), name="update_teacher"),
    path("delete_teachers/<uuid:uuid>", DeleteTeacherView.as_view(), name="delete_teacher"),
]
