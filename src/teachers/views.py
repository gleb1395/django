from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy

from django.views.generic import CreateView, UpdateView, DeleteView

from teachers.forms import TeacherForm
from teachers.models import Teacher

from webargs import fields
from webargs.djangoparser import use_args


@login_required
@use_args(
    {
        "first_name": fields.Str(
            required=False,
        ),
        "last_name": fields.Str(
            required=False,
        ),
        "salary": fields.Float(
            required=False,
        ),
        "search": fields.Str(required=False),
    },
    location="query",
)
def get_teachers(request: HttpRequest, params) -> HttpResponse:
    teachers = Teacher.objects.all()

    search_fields = ["first_name", "last_name", "salary"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__contains": param_value})
            teachers = teachers.filter(or_filter)
        else:
            teachers = teachers.filter(**{param_name: param_value})
    return render(request, template_name="teachers/templates/list_teacher.html", context={"teachers": teachers})


class CreateTeacherView(LoginRequiredMixin, CreateView):
    template_name = "teachers/templates/create_teacher.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:get_teachers")


class UpdateTeacherView(LoginRequiredMixin, UpdateView):
    template_name = "teachers/templates/update_teacher.html"
    form_class = TeacherForm
    success_url = reverse_lazy("teachers:get_teachers")
    queryset = Teacher.objects.all()
    pk_url_kwarg = "uuid"


class DeleteTeacherView(LoginRequiredMixin, DeleteView):
    template_name = "teachers/templates/delete_teacher.html"
    model = Teacher
    fields = ["first_name", "last_name", "email", "years_of_experience", "speciality", "group", "image_teacher"]
    queryset = Teacher.objects.all()
    success_url = reverse_lazy("teachers:get_teachers")
    pk_url_kwarg = "uuid"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        teacher = self.get_object()
        context["teacher_name"] = f"{teacher.first_name} {teacher.last_name}"
        return context
