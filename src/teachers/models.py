import random
from uuid import uuid4

from django.db import models
from faker import Faker

from basic.models import Person
from groups.models import Group


class Teacher(Person):
    uuid = models.UUIDField(default=uuid4, primary_key=True, unique=True, editable=False, db_index=True)
    salary = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    years_of_experience = models.PositiveSmallIntegerField(blank=True, null=True)
    speciality = models.CharField(max_length=200, blank=True, null=True)
    group = models.ManyToManyField(Group)
    image_teacher = models.ImageField(blank=True, null=True, upload_to="static/teachers/img/")

    @classmethod
    def generate_teacher(cls, count: int) -> None:
        faker = Faker()
        group_queryset = Group.objects.all()[:3]
        group_list = list(group_queryset)
        for i in range(count):
            teacher = Teacher.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                salary=faker.pyfloat(
                    left_digits=None,
                    right_digits=2,
                    positive=True,
                    min_value=20000,
                    max_value=100000,
                ),
                years_of_experience=faker.pyint(min_value=1, max_value=15),
                speciality=faker.job(),
            )
            group_samples = random.sample(group_list, 2)
            teacher.group.set(group_samples)

    def __str__(self):
        return (
            f"{self.pk} {self.first_name} {self.last_name} {self.email} {self.years_of_experience} "
            f"{self.speciality} {self.salary} {self.years_of_experience} {self.speciality}"
        )
