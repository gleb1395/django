from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm

from groups.models import Group
from students.models import Student


# class StudentForm(ModelForm):
#     group = forms.ModelChoiceField(queryset=Group.objects.all(), to_field_name='name_group')
#
#     class Meta:
#         model = Student
#         fields = ["first_name", "last_name", "email", "group", "image_student", "resume_student"]


class StudentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(StudentForm, self).__init__(*args, **kwargs)
        self.fields["group"].queryset = Group.objects.all()
        self.fields["group"].to_field_name = "name_group"
        self.fields["group"].label_from_instance = lambda obj: "%s" % obj.name_group

    group = forms.ModelChoiceField(queryset=Group.objects.none(), to_field_name="start_time")

    class Meta:
        model = Student
        fields = ["first_name", "last_name", "email", "group", "image_student", "resume_student"]

    @staticmethod
    def normalize_text(text):
        return text.strip().capitalize()

    def clean_last_name(self):
        return self.normalize_text(self.cleaned_data["last_name"])

    def clean_first_name(self):
        return self.normalize_text(self.cleaned_data["first_name"])

    def clean_email(self):
        email = self.cleaned_data["email"]

        if "@yandex" in email.lower():
            raise ValidationError("Yandex email is forbidden to use in our country")
        return email

    def clean(self):
        cleaned_data = super().clean()

        first_name = cleaned_data["first_name"]
        last_name = cleaned_data["last_name"]

        if first_name == last_name:
            raise ValidationError("First name and last name cannot be equal")
        return cleaned_data
