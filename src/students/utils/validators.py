from django.core.exceptions import ValidationError


def first_name_validator(first_name: str) -> None:
    if "Vova" in first_name:
        raise ValidationError("Vova is not correct name, should be Volodymyr")
