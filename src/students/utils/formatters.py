def format_records(records: list) -> str:
    return "<br>".join(str(records) for records in records)
