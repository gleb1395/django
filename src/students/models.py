import os  # NOQA F401
from datetime import datetime
from uuid import uuid4

from django.db import models
from faker import Faker

from basic.models import Person
from groups.models import Group


class Student(Person):
    uuid = models.UUIDField(default=uuid4, primary_key=True, unique=True, editable=False, db_index=True)
    grade = models.PositiveSmallIntegerField(default=0, null=True)
    birth_date = models.DateField(blank=True, null=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    image_student = models.ImageField(blank=True, null=True, upload_to="static/student/img/")
    resume_student = models.FileField(blank=True, null=True, upload_to="static/student/pdf/")

    @classmethod
    def generate_student(cls, count: int) -> None:
        faker = Faker()
        groups = Group.objects.all()
        for i in range(count):
            student = Student.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_of_birth(minimum_age=16, maximum_age=60),
                group=groups.first(),
            )
            student.save()

    def __str__(self) -> str:
        return f"{self.pk} {self.first_name} {self.last_name} {self.email} {self.birth_date} {self.image_student}"

    def age(self):
        return datetime.now().year - self.birth_date.year
