from django.urls import path
from students.views import get_student, CreateStudentView, UpdateStudentView, DeleteStudentView

app_name = "students"

urlpatterns = [
    path("", get_student, name="get_student"),
    path("create/", CreateStudentView.as_view(), name="create_student"),
    path("update/<uuid:uuid>", UpdateStudentView.as_view(), name="update_student"),
    path("delete/<uuid:uuid>", DeleteStudentView.as_view(), name="delete_student"),
]
