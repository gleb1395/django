from django import forms  # NOQA F401
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView

from students.models import Student

from webargs import fields
from webargs.djangoparser import use_args

from students.forms import StudentForm
from students.utils.formatters import format_records  # NOQA F401


class IndexView(TemplateView):
    template_name = "index.html"


@login_required
@use_args(
    {
        "first_name": fields.Str(
            required=False,
        ),
        "last_name": fields.Str(
            required=False,
        ),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_student(request: HttpRequest, params) -> HttpResponse:
    students = Student.objects.all()

    search_fields = ["first_name", "last_name", "email"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__contains": param_value})
            students = students.filter(or_filter)
        else:
            students = students.filter(**{param_name: param_value})
    return render(request, template_name="student/templates/list.html", context={"students": students})


class CreateStudentView(LoginRequiredMixin, CreateView):
    template_name = "student/templates/create.html"
    # model = Student
    # fields = ["first_name", "last_name", "email", "group", "image_student", "resume_student"]
    form_class = StudentForm
    success_url = reverse_lazy("students:get_student")


class UpdateStudentView(LoginRequiredMixin, UpdateView):
    form_class = StudentForm
    template_name = "student/templates/update.html"
    queryset = Student.objects.all()
    success_url = reverse_lazy("students:get_student")
    pk_url_kwarg = "uuid"


class DeleteStudentView(LoginRequiredMixin, DeleteView):
    model = Student
    fields = ["first_name", "last_name", "email", "group", "image_student", "resume_student"]
    template_name = "student/templates/delete.html"
    queryset = Student.objects.all()
    success_url = reverse_lazy("students:get_student")
    pk_url_kwarg = "uuid"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        student = self.get_object()
        context["student_name"] = f"{student.first_name} {student.last_name}"
        return context


@csrf_exempt
def handling_404(request: HttpRequest, exception: Exception) -> HttpResponse:
    return render(request, template_name="404.html")
